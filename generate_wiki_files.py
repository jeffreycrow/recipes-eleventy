import os
import re
import requests
import urllib.parse
import frontmatter

from pprint import pprint

DOWNLOAD_DIR='./src'

def preprocessContent(pageObj):
	pageObj['content'] = addHeadings(pageObj['content'])
	pageContent = addFrontmatter(pageObj)

	return pageContent


def addFrontmatter(pageObj):
	slugRegex = r'([^\/]*)/'
	try:
		slug = re.match(slugRegex, pageObj['slug'])[1]
	except IndexError:
		print('Slug match error, can\'t write frontmatter')
		return false


	slugStr = '{{ \"'+pageObj['title']+'\" | slug }}'
	
	post = frontmatter.loads(pageObj['content'])
	post['layout'] = 'layouts/recipe.njk'
	post['title'] = pageObj['title']
	post['type'] = slug
	post['tags'] = ['recipe', slug]
	post['permalink'] = '/'+slugStr+'/'
	return frontmatter.dumps(post)


def addHeadings(content):
	sectionRegex = '(?<!#)#(?!#)\s*([^\n]+)\n'
	sectionTag = r"{% endSection %}\n{% startSection '\1' %}\n"
	processedContent = re.sub(sectionRegex, sectionTag, content)
	processedContent = processedContent.replace("{% endSection %}", "", 1)
	processedContent += "\n{% endSection %}\n"
	return processedContent


def main():
	if not os.path.exists(DOWNLOAD_DIR):
		os.mkdir(DOWNLOAD_DIR)

	apiRoot = 'https://gitlab.com/api/v4/projects/'
	projectName = urllib.parse.quote_plus('jeffreycrow/recipes')

	print('Making GitLab Wiki API call...')
	listRequest = requests.get(apiRoot+projectName+'/wikis')
	listArr = listRequest.json()

	for page in listArr:
		if page['slug'] == 'home':
			continue

		encodedPageName = urllib.parse.quote_plus(page['slug'])
		path = os.path.split(page['slug'])
		try:
			os.makedirs(DOWNLOAD_DIR+'/'+path[0])
		except FileExistsError:
			pass

		pageRequest = requests.get(apiRoot+projectName+'/wikis/'+encodedPageName)
		processedContent = preprocessContent(pageRequest.json())

		f = open(DOWNLOAD_DIR+'/'+page['slug']+'.md', 'w')
		print('Writing %s' % page['slug'])
		f.write(processedContent)
		f.close()


main()
