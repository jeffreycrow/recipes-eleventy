# This is my recipe collection.

Install dependencies:
```
$ pipenv install
$ npm install
```

Download wiki content locally:
```
$ pipenv run python generate_wiki_files.py
```

Run locally:
```
$ npm run dev:debug
```

Build for production:
```
$ npm run build:prod
```

# Details

Built with [11ty](https://www.11ty.dev/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Recipes are stored in the [wiki](https://gitlab.com/jeffreycrow/recipes/-/wikis/home) for this project as Markdown files.

A CI job runs every time the wiki is updated using the wiki update webhook.

First, a Python script pulls down the recipes from the wiki and turns them into individual files. I would have liked to keep everything in Node but I already had the code to do that in Python so I used it.

Then, it does some light processing, generating Markdown frontmatter and adding shortcodes to delineate sections that will be turned into HTML section tags by 11ty.

Next, 11ty turns all the Markdown files into HTML.

Finally, that directory of HTML files is pushed to GitLab Pages.